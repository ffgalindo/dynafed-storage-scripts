#!/usr/bin/env python

###########################################################################
#
# cephrgw-staraccounting
#
# Heavily based on the star-accounting.py script by Fabrizion Furano
# http://svnweb.cern.ch/world/wsvn/lcgdm/lcg-dm/trunk/scripts/StAR-accounting/star-accounting.py
#
# A script that produces an accounting record following the EMI StAR specs
# utilizing the readosgw-admin REST API.
#
#
# Prerequisites:
# boto modules from your distribution
# lxml modules
# radosgw-admin modules:
#   pip install radosgw-admin
#
# User in CEPH with read capabilities for buckets. Example:
# radosgw-admin user create --uid=accounting --display-name="Accounting"
# radosgw-admin caps add --uid=accounting --caps "buckets=read"
#
# Syntax:
#
# cephrgw-staraccounting [-h] [--help]
# .. to get the help screen
#
#
#  v0.0.1 initial test

__version__ = "0.0.1"
__author__  = "Fernando Fernandez Galindo"


import boto
import datetime
#import radosgw
import radosgw.connection as rgwcon
import sys
import socket
import uuid
from lxml import etree
from optparse import OptionParser,OptionGroup



#################
## Definitions ##
#################

def addrecord(xmlroot, bucket, hostname, group, user, site, validduration, recordid = None):
  # update XML
  rec = etree.SubElement(xmlroot, SR+'StorageUsageRecord')
  rid = etree.SubElement(rec, SR+'RecordIdentity')
  rid.set(SR+"createTime", datetime.datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%SZ"))

  # Set the bucket name as the "StorageShare field"
  if bucket.bucket:
    sshare = etree.SubElement(rec, SR+"StorageShare")
    sshare.text = bucket.bucket

  if hostname:
    ssys = etree.SubElement(rec, SR+"StorageSystem")
    ssys.text = hostname

  recid = recordid
  if not recid:
    recid = hostname+"-"+str(uuid.uuid1())
  rid.set(SR+"recordId", recid)

  subjid = etree.SubElement(rec, SR+'SubjectIdentity')

  if group:
    grouproles = group.split('/')

    # If the last token is Role=... then we fetch the role and add it to the record
    tmprl = grouproles[-1]
    if tmprl.find('Role=') != -1:
      splitroles = tmprl.split('=')
      if (len(splitroles) > 1):
        role = splitroles[1]
        grp = etree.SubElement(subjid, SR+"GroupAttribute" )
        grp.set( SR+"attributeType", "role" )
        grp.text = role
      # Now drop this last token, what remains is the vo identifier
      grouproles.pop()


    # The voname is the first token
    voname = grouproles.pop(0)
    grp = etree.SubElement(subjid, SR+"Group")
    grp.text = voname

    # If there are other tokens, they are a subgroup
    if len(grouproles) > 0:
      subgrp = '/'.join(grouproles)
      grp = etree.SubElement(subjid, SR+"GroupAttribute" )
      grp.set( SR+"attributeType", "subgroup" )
      grp.text = subgrp

  if user:
    usr = etree.SubElement(subjid, SR+"User")
    usr.text = user

  if site:
    st = etree.SubElement(subjid, SR+"Site")
    st.text = site

  e = etree.SubElement(rec, SR+"StorageMedia")
  e.text = "disk"

  if validduration:
    e = etree.SubElement(rec, SR+"StartTime")
    d = datetime.datetime.utcnow() - datetime.timedelta(seconds=validduration)
    e.text = d.strftime("%Y-%m-%dT%H:%M:%SZ")

  e = etree.SubElement(rec, SR+"EndTime")
  e.text = datetime.datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%SZ")


  if bucket.usage.num_objects:
    e = etree.SubElement(rec, SR+"FileCount")
    e.text = str(bucket.usage.num_objects)


  if not bucket.usage.size_actual:
    bucket.usage.size_actual = 0

  e1 = etree.SubElement(rec, SR+"ResourceCapacityUsed")
  e1.text = str(bucket.usage.size_actual)

  e3 = etree.SubElement(rec, SR+"ResourceCapacityAllocated")
  e3.text = str(bucket.bucket_quota['max_size'])

  if not bucket.usage.size_utilized:
    bucket.usage.size_utilized = 0

  e2 = etree.SubElement(rec, SR+"LogicalCapacityUsed")
  e2.text = str(bucket.usage.size_utilized)

def gethostname():
  fullname = socket.gethostname()
  if '.' not in fullname:
    fullname = resolve(fullname)
  return fullname



###############
## Main Code ##
###############

usage = "usage: %prog [options] api-hostname"
parser = OptionParser(usage)

parser.add_option('-v', '--verbose', dest='verbose', action='count', help='Increase verbosity level for debugging this script (on stderr)')

group = OptionGroup(parser, "Ceph RadosGW Admin API options")
group.add_option('--accesskey', dest='accesskey', action='store', default=False, help="RadosGW Accounting user Access Key.")
group.add_option('--secretkey', dest='secretkey', action='store', default=False, help="RadosGW Accounting user Secret Key.")
group.add_option('--awssignature', dest='awssignature', action='store', default='AWS4', help="Defaults to AWS4, AWS2 is also available.")
group.add_option('--debug', dest='debug', action='store_true', default=False, help="Set to enable debug mode for boto AWS connection.")
group.add_option('--path', dest='path', action='store', default='/admin', help="Path to the RadowGW Admin API. Default: '/admin'.")
group.add_option('--port', dest='port', action='store', default=None, help="Specify non-standard port to the RadowGW Admin API.")
group.add_option('--proxy', dest='proxy', action='store', default=None, help="Proxy server to use if needed.")
group.add_option('--proxy-port', dest='proxyport', action='store', default=None, help="Port to connect to the Proxy server.")
group.add_option('--proxy-user', dest='proxyuser', action='store', default=None, help="User to authenticate to the Proxy server.")
group.add_option('--proxy-pass', dest='proxypass', action='store', default=None, help="Password to authenticate to the Proxy server.")
group.add_option('--ssl', dest='ssl', action='store_true', default=False, help="Declare to enable SSL connection.")
group.add_option('--timeout', dest='timeout', action='store', default=30, help="Connection timeout in seconds. Default: 30")
group.add_option('--validatecerts', dest='validatecerts', action='store_true', default=False, help="Declare to validate certs when SSL is enabled.")
parser.add_option_group(group)

group = OptionGroup(parser, "XML format options")
group.add_option('--reportgroups', dest='reportgroups', action='store_true', default=False, help="Report about all groups")
group.add_option('--reportusers', dest='reportusers', action='store_true', default=False, help="Report about all users")
group.add_option('--hostname', dest='hostname', action='store', default='', help="The hostname string to use in the record. If not specified, api-hostname arg us used.")
group.add_option('--site', dest='site', action='store', default="", help="The site string to use in the record. Default: none.")
group.add_option('--recordid', dest='recordid', action='store', default=None, help="The recordid string to use in the record. Default: a newly computed unique string.")
group.add_option('--validduration', dest='validduration', action='store', default=86400, help="Valid duration of this record, in seconds (default: 1 day)")
parser.add_option_group(group)

options, args = parser.parse_args()

if len(args) != 1:
    parser.error("No RGW API address/hostname specified.")

if options.hostname:
    host = options.hostname
else:
    host = sys.argv[1]

### For easy testing, remove once good
options.accesskey = "XEFU9YOKWHY0B2MD0BBY"
options.secretkey = "kITkmqdp9GyHD71x5r9q9TJ05tB1ygf3Jybt2hvZ"
options.site      = 'dynafed-TRIUMF'

# Get a connection to RadosGW host
rgwadmin =  rgwcon.RadosGWAdminConnection(
                host=sys.argv[1],
                access_key=options.accesskey,
                secret_key=options.secretkey,
                admin_path=options.path,
                aws_signature=options.awssignature,
                timeout=options.timeout,
                is_secure=options.ssl,
                port=options.port,
                proxy=options.proxy,
                proxy_port=options.proxyport,
                proxy_user=options.proxyuser,
                proxy_pass=options.proxypass,
                debug=options.debug,
                validate_certs=options.validatecerts
            )

# Obtain list of all available buckets
buckets = rgwadmin.get_buckets()

# Init the xml generator
SR_NAMESPACE = "http://eu-emi.eu/namespaces/2011/02/storagerecord"
SR = "{%s}" % SR_NAMESPACE
NSMAP = {"sr": SR_NAMESPACE}
xmlroot = etree.Element(SR+"StorageUsageRecords", nsmap=NSMAP)

# Create xml file with each bucket as a StorageUsageRecord object
for bucket in buckets:
    addrecord(xmlroot,
              bucket,
              hostname=host,
              group=None,
              user=None,
              site=options.site,
              validduration=options.validduration,
              recordid=options.recordid)

s = etree.tostring(xmlroot, pretty_print=True)
print('')
print(s)
print('')
