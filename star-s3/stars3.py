#!/usr/bin/env python

"""
A script that produces an accounting record following the EMI StAR specs for
buckets in a Ceph S3 cluster utilizing the readosgw-admin REST API.

Prerequisites:
  - boto (used by radosgw-admin)
  - lxml
  - radosgw-admin modules:
      pip install radosgw-admin

  User in CEPH with read capabilities for buckets. Example:
    radosgw-admin user create --uid=accounting --display-name="Accounting"
    radosgw-admin caps add --uid=accounting --caps "buckets=read"

Usage:

  User needs to provide the accesskey and secretkey for the Admin API user,
  the site (to fill in the "StorageSite" StAR field), the URL to the RadosGW
  and an output directory.

  The user needs to specify the Buckets to gather the information from. This
  can be done with the --bucket/-b (which can be called multiple times, one for
  each bucket) and --file/-f (which should point to a text file with a list
  of bucket names, one per line). Both options can be used simultaneously.

Syntax:
  stars3 [-h] [--help] to get the help screen
  stars3.py [options] --hostname api-hostname

Examples:

  ./stars3.py --accesskey XXXXX \
              --secretkey XXXXX \
              --site dynafed-TRIUMF \
              --hostname atlas-fed-rgw01.triumf.ca \
              -b bucket_name1 \
              -b bucket_name2 \
              -f ./bucketlist.txt \
              --dir ./output-dir


v0.0.1 initial test
v0.1.0 working in one specific circumstance.
v0.1.1 Instead of all buckets availalbe, user needs to choose by using
      -b --bucket option, or passing a textfile with a list with -f --file.
v0.2.0 Turned endpoint stats into objects and object methods.
"""

from __future__ import print_function

__version__ = "0.2.0"
__author__ = "Fernando Fernandez Galindo"

import datetime
import sys
import uuid
from optparse import OptionParser, OptionGroup

from lxml import etree
import radosgw.connection as rgwcon

################
## Help/Usage ##
################

usage = "usage: %prog [options] api-hostname"
parser = OptionParser(usage)

#parser.add_option('-v', '--verbose', dest='verbose', action='count', help='Increase verbosity level for debugging this script (on stderr)')

group = OptionGroup(parser, "S3 options")
group.add_option('--accesskey', dest='accesskey', action='store', default=None, help="User Access Key.")
group.add_option('--secretkey', dest='secretkey', action='store', default=None, help="User Secret Key.")
group.add_option('--awssignature', dest='awssignature', action='store', default='AWS4', help="Defaults to AWS4, AWS2 is also available.")
group.add_option('-b', '--bucket', dest='buckets', action='append', default=[], help="Specify the bucket name. Use multiple times for each bucket.")
group.add_option('-f', '--file', dest='file', action='store', default=None, help="Text file containing a list of bucket names.")
group.add_option('--hostname', dest='hostname', action='store', default='', help="The hostname string to use in the record. If not specified, api-hostname arg us used.")
parser.add_option_group(group)

group = OptionGroup(parser, "Ceph RadosGW Admin API options")
group.add_option('--debug', dest='debug', action='store_true', default=False, help="Set to enable debug mode for boto AWS connection.")
group.add_option('--path', dest='path', action='store', default='/admin', help="Path to the RadowGW Admin API. Default: '/admin'.")
group.add_option('--port', dest='port', action='store', default=None, help="Specify non-standard port to the RadowGW Admin API.")
group.add_option('--proxy', dest='proxy', action='store', default=None, help="Proxy server to use if needed.")
group.add_option('--proxy-port', dest='proxyport', action='store', default=None, help="Port to connect to the Proxy server.")
group.add_option('--proxy-user', dest='proxyuser', action='store', default=None, help="User to authenticate to the Proxy server.")
group.add_option('--proxy-pass', dest='proxypass', action='store', default=None, help="Password to authenticate to the Proxy server.")
group.add_option('--ssl', dest='ssl', action='store_true', default=False, help="Declare to enable SSL connection.")
group.add_option('--timeout', dest='timeout', action='store', default=30, help="Connection timeout in seconds. Default: 30")
group.add_option('--validatecerts', dest='validatecerts', action='store_true', default=False, help="Declare to validate certs when SSL is enabled.")
parser.add_option_group(group)

group = OptionGroup(parser, "Output Options")
group.add_option('--dir', '--directory', dest='directory', action='store', default='./.', help="Path to directory for file output. Default: PWD")
group.add_option('--reportgroups', dest='reportgroups', action='store_true', default=False, help="Report about all groups")
group.add_option('--reportusers', dest='reportusers', action='store_true', default=False, help="Report about all users")
group.add_option('--site', dest='site', action='store', default="", help="The site string to use in the record. Default: none.")
group.add_option('--storagemedia', dest='storagemedia', action='store', default='disk', help="Set type of storage media (disk, tape). Default: disk")
group.add_option('--recordid', dest='recordid', action='store', default=None, help="The recordid string to use in the record. Default: a newly computed unique string.")
group.add_option('--validduration', dest='validduration', action='store', default=86400, help="Valid duration of this record, in seconds (default: 1 day)")
group.add_option('--json', dest='json', action='store_true', default=False, help="Set this flag to ouput a JSON file.")
group.add_option('--xml', dest='xml', action='store_true', default=False, help="Set this flag to ouput an XML file.")

parser.add_option_group(group)

options, args = parser.parse_args()

#if len(args) != 1:
#    parser.error("No storage type specified.")


#############
## Classes ##
#############

class s3Stats(object):
    def __init__(self, options, bucket):
        self.bucket = bucket

    def make_StAR_xml(self):
        """
        Heavily based on the star-accounting.py script by Fabrizion Furano
        http://svnweb.cern.ch/world/wsvn/lcgdm/lcg-dm/trunk/scripts/StAR-accounting/star-accounting.py
        """
        SR_namespace = "http://eu-emi.eu/namespaces/2011/02/storagerecord"
        SR = "{%s}" % SR_namespace
        NSMAP = {"sr": SR_namespace}
        xmlroot = etree.Element(SR+"StorageUsageRecords", nsmap=NSMAP)

        # update XML
        rec = etree.SubElement(xmlroot, SR+'StorageUsageRecord')
        rid = etree.SubElement(rec, SR+'RecordIdentity')
        rid.set(SR+"createTime", datetime.datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%SZ"))

        # Set the bucket name as the "StorageShare field"
        if endpoint.storageshare:
            sshare = etree.SubElement(rec, SR+"StorageShare")
            sshare.text = endpoint.storageshare

        if endpoint.hostname:
            ssys = etree.SubElement(rec, SR+"StorageSystem")
            ssys.text = endpoint.hostname

        recid = endpoint.recordid
        if not recid:
            recid = endpoint.hostname+"-"+str(uuid.uuid1())
        rid.set(SR+"recordId", recid)

        subjid = etree.SubElement(rec, SR+'SubjectIdentity')

    #    if endpoint.group:
    #      grouproles = endpoint.group.split('/')
    #      # If the last token is Role=... then we fetch the role and add it to the record
    #    tmprl = grouproles[-1]
    #    if tmprl.find('Role=') != -1:
    #      splitroles = tmprl.split('=')
    #      if (len(splitroles) > 1):
    #        role = splitroles[1]
    #        grp = etree.SubElement(subjid, SR+"GroupAttribute" )
    #        grp.set( SR+"attributeType", "role" )
    #        grp.text = role
    #      # Now drop this last token, what remains is the vo identifier
    #      grouproles.pop()
    #
    #    # The voname is the first token
    #    voname = grouproles.pop(0)
    #    grp = etree.SubElement(subjid, SR+"Group")
    #    grp.text = voname
    #
    #    # If there are other tokens, they are a subgroup
    #    if len(grouproles) > 0:
    #      subgrp = '/'.join(grouproles)
    #      grp = etree.SubElement(subjid, SR+"GroupAttribute" )
    #      grp.set( SR+"attributeType", "subgroup" )
    #      grp.text = subgrp
    #
    #    if endpoint.user:
    #      usr = etree.SubElement(subjid, SR+"User")
    #      usr.text = endpoint.user

        if endpoint.site:
            st = etree.SubElement(subjid, SR+"Site")
            st.text = endpoint.site

        # too many e vars here below, wtf?
        if endpoint.storagemedia:
            e = etree.SubElement(rec, SR+"StorageMedia")
            e.text = endpoint.storagemedia

        if endpoint.validduration:
            e = etree.SubElement(rec, SR+"StartTime")
            d = datetime.datetime.utcnow() - datetime.timedelta(seconds=endpoint.validduration)
            e.text = d.strftime("%Y-%m-%dT%H:%M:%SZ")

        e = etree.SubElement(rec, SR+"EndTime")
        e.text = datetime.datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%SZ")


        if endpoint.filecount:
            e = etree.SubElement(rec, SR+"FileCount")
            e.text = str(endpoint.filecount)


        if not endpoint.resourcecapacityused:
            endpoint.resourcecapacityused = 0

        e1 = etree.SubElement(rec, SR+"ResourceCapacityUsed")
        e1.text = str(endpoint.resourcecapacityused)

        e3 = etree.SubElement(rec, SR+"ResourceCapacityAllocated")
        e3.text = str(endpoint.resourcecapacityallocated)

        if not endpoint.logicalcapacityused:
            endpoint.logicalcapacityused = 0

        e2 = etree.SubElement(rec, SR+"LogicalCapacityUsed")
        e2.text = str(endpoint.logicalcapacityused)

        return xmlroot

class rgwStats(s3Stats):

    def get_stats(self):
        connection = rgwcon.RadosGWAdminConnection(
            host=options.hostname,
            access_key=options.accesskey,
            secret_key=options.secretkey,
            admin_path=options.path,
            aws_signature=options.awssignature,
            timeout=options.timeout,
            is_secure=options.ssl,
            port=options.port,
            proxy=options.proxy,
            proxy_port=options.proxyport,
            proxy_user=options.proxyuser,
            proxy_pass=options.proxypass,
            debug=options.debug,
            validate_certs=options.validatecerts
        )

        # Set bucket data into StARVars
        # Get bucket information
        b = connection.get_bucket(bucket)
        self.filecount = b.usage.num_objects
        self.hostname = options.hostname
        self.logicalcapacityused = b.usage.size_utilized
        self.recordid = options.site +"_"+ options.storagemedia +"_"+ b.bucket
        self.resourcecapacityallocated = b.bucket_quota['max_size']
        self.resourcecapacityused = b.usage.size_utilized
        self.site = options.site
        self.storagemedia = options.storagemedia
        self.storageshare = b.bucket
        self.validduration = options.validduration

        return self

###############
## Functions ##
###############

#def gethostname():
#    fullname = socket.gethostname()
#    if '.' not in fullname:
#        fullname = resolve(fullname)
#    return fullname

###############
## Main Code ##
###############
##Test Vars
options.accesskey = 'XEFU9YOKWHY0B2MD0BBY'
options.secretkey = 'kITkmqdp9GyHD71x5r9q9TJ05tB1ygf3Jybt2hvZ'
options.hostname = 'atlas-fed-rgw01.triumf.ca'
options.buckets = ['triumf-ceph-s3-b01']


# Check that buckets were defined by the user.
# Append any bucket names from file if given
if options.file:
    try:
        with open(options.file, 'r') as f:
            for line in f:
                line = line.strip()
                options.buckets.append(line)
    except IOError:
        print('\nERROR: Could not read file:', options.file, '\n')
        sys.exit(1)
# Check that buckets have been defined:
if not options.buckets:
    print('\nError: No buckets defined. Use -h for help.\n')
    sys.exit(1)
endpoints = []

for bucket in options.buckets:
    endpoint = rgwStats(options, bucket)
    endpoints.append(endpoint.get_stats())


for endpoint in endpoints:
    if options.json:
        pass

    elif options.xml:
        xmlroot = endpoint.make_StAR_xml()
        s = etree.tostring(xmlroot, pretty_print=True)
        print(s)

        filename = options.directory + '/' + endpoint.recordid + '.xml'
        output = open(filename, 'w')
        output.write(s)
        output.close()
