A script that produces an accounting record following the EMI StAR specs for
buckets in a Ceph S3 cluster utilizing the readosgw-admin REST API.

## Prerequisites:

boto modules from your distribution
lxml modules
radosgw-admin modules:
 pip install radosgw-admin

## Ceph prerequisites

### Create user with Admin API read permissions

User in CEPH with read capabilities for buckets. Example:

```bash
radosgw-admin user create --uid=accounting --display-name="Accounting"
radosgw-admin caps add --uid=accounting --caps "buckets=read"
```

## Usage

User needs to provide the accesskey and secretkey for the Admin API user,
the site (to fill in the "StorageSite" StAR field), the URL to the RadosGW
and an output directory.

The user needs to specify the Buckets to gather the information from. This
can be done with the --bucket/-b (which can be called multiple times, one for
each bucket) and --file/-f (which should point to a text file with a list
of bucket names, one per line). Both options can be used simultaneously.

Example:
```bash
./stars3.py --accesskey XXXXX \
            --secretkey XXXXX \
            --site dynafed-TRIUMF \
            --hostname atlas-fed-rgw01.triumf.ca \
            -b bucket_name1 \
            -b bucket_name2 \
            -f ./bucketlist.txt \
            --dir ./output-dir
```
