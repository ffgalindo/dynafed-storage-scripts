Script able to aggregate storage StAR .xml files located at local path or
a remote URL location, with extra options if dealing with a Dynafed intance.

## Prerequisites

Install the following modules which should be available in most distributions
package manager:

requests:
```
yum install python2-requests
```
lxml:
```
yum install python-lxml
```
## Usage

This script functions in two modalities:

__--local__

Use when the files are located in the filesystem. In this mode a list with
filenames (-f/--files) or directory path (-d/--dir) containting the .xml
files can be used. They are mutually exclusive.

__--remote__

Use when files are located on a website. It is able to handle HTTPS, X509
client authentication and server verificatoin (as well as ignoring it).
If the website is a Dynafed instance, the --ugrconfig option can be used.
The filepath to the endpoints.conf file which contains the Storage Endpoints
will be used to figure out where to look for the files in the namespace.
The opton --dfprefix is needed if the Dynafed namespace is not mounted in the
default "/myfed"

For output, a couple of options are avaialbe:

-o/--outfile stdout -> Prints output to standar output.

-o/--outfile filename -> Writes output to the filename specified

-m/--memcached -> Uploads information to memcached. Use --memhost, --memport
                      if not using the default localhost:11211

The local files can be specified either by passing a local directory with all
the .xml:

```bash
./staragg.py --local -d /path/to/directory
```

or with a text file with a full path + filename for each file to be aggegated:

```bash
./staragg.py --local -f list.txt
```

The script is intended to work with UGR's endpoints.conf files to extract the
location and filename for each of the configured endpoints. User key and
certificates can be specified for client authentication and CA to verify
server certificate. Specify the "-m" option to
upload to memcached (see help for other options):

```bash
./staragg.py --remote
             --ugrconfig /etc/ugr/conf.d/endpoints.conf \
             --hostname https://dyanfed:443 \
             --dfprefix /myfed \
             --key userkey.pem \
             --cert usercert.pem \
             --cafile /etc/grid-security/certificates \
             -o output.xml \
             -m
```
