#!/usr/bin/env python
"""
A script intended agregate any number of StAR .xml files into a single
multi StorageUsageRecord one.

This script functions in two modalities:
--local
    Use when the files are located in the filesystem. In this mode a list with
    filenames (-f/--files) or directory path (-d/--dir) containting the .xml
    files can be used. They are mutually exclusive.

--remote
    Use when files are located on a website. It is able to handle HTTPS, X509
    client authentication and server verificatoin (as well as ignoring it).
    If the website is a Dynafed instance, the --ugrconfig option can be used.
    The filepath to the endpoints.conf file which contains the Storage Endpoints
    will be used to figure out where to look for the files in the namespace.
    The opton --dfprefix is needed if the Dynafed namespace is not mounted in the
    default "/myfed"

For output, a couple of options are avaialbe:
    -o/--outfile stdout -> Prints output to standar output.
    -o/--outfile filename -> Writes output to the filename specified

    -m/--memcached -> Uploads information to memcached. Use --memhost, --memport
                      if not using the default localhost:11211

Prerequisites:
    Modules:
    - requests
    - lxml

Syntax:
    staragg.py [-h] [--help] to get the help screen
    staragg.py (--local|--remote) [options] [output-options]

Examples:

    ./staragg.py --local -d /path/to/directory

    ./staragg.py --local -f list.txt

    ./staragg.py \
             --remote
             --ugrconfig /etc/ugr/conf.d/endpoints.conf \
             --hostname https://dyanfed:443 \
             --dfprefix /myfed \
             --key userkey.pem \
             --cert usercert.pem \
             --cafile /etc/grid-security/certificates \
             -o output.xml \
             -m
"""

#
#  v0.0.1 initial test
#  v0.0.2 added URL support
#  v0.0.3 added SSL client auth and server cert verification.
#  v0.1.0 added support for both remote and local files. Names are obtained
#         from ugr's endpoints.conf file.
#  v0.2.0 exchanged urllib2 and httplib in favor of requests module
#  v0.3.0 added default output to stdout, options to add memcache and a file
#  v0.3.1 added file list from text files and globbing from directory

from __future__ import print_function

__version__ = "0.3.1"
__author__ = "Fernando Fernandez Galindo"

import copy
import glob
from optparse import OptionParser, OptionGroup
import sys

from lxml import etree
import requests

################
## Help/Usage ##
################

usage = "usage: %prog [options] api-hostname"
parser = OptionParser(usage)

#parser.add_option('-v', '--verbose', dest='verbose', action='count', help='Increase verbosity level for debugging this script (on stderr)')
parser.add_option('--local', dest='islocal', action='store_true', default=True, help='Declare to parse files in local filesystem')
parser.add_option('--remote', dest='islocal', action='store_false', help='Declare to parse files in a remote URL')
parser.add_option('--ugrconfig', dest='ugrconfig', action='store', default=None, help="Declare to parse obtain file information from UGR's config file")

group = OptionGroup(parser, "Local options")
group.add_option('-f', '--files', dest='files', action='store', default=None, help="Text file containing a list of files to aggregate")
group.add_option('-d', '--dir', dest='directory', action='store', default='./.', help="Path to the directory where the files are stored. Ignored if 'files' option is used")
parser.add_option_group(group)

group = OptionGroup(parser, "Remote options")
group.add_option('--cafile', dest='sslverify', action='store', default=True, help="File containing CA certificate. If unspecified, OS defaults will be loaded.")
group.add_option('--capath', dest='sslverify', action='store', help="Path to folder containing CA certificates. If unspecified, OS defaults will be loaded.")
group.add_option('--cert', dest='cert', action='store', default=None, help="Path to client certificate.")
group.add_option('--dfprefix', dest='dfprefix', action='store', default='/myfed', help='The URL prefix where Dynafed mounts the endpoints. Default "/myfed"')
group.add_option('--hostname', dest='hostname', action='store', default='', help="The hostname string to use in the record. If not specified, api-hostname arg us used.")
group.add_option('--key', dest='key', action='store', default=None, help="Path to client certificate private key")
group.add_option('--noverify', dest='sslverify', action='store_false', help="Declare to disable server certificate verification.")
parser.add_option_group(group)

group = OptionGroup(parser, "Memcached options")
group.add_option('--memhost', dest='memhost', action='store', default='127.0.0.1', help="IP or hostname of memcached instance. Default: 127.0.0.1")
group.add_option('--memport', dest='memport', action='store', default='11211', help="Port tof memcached instance. Default: 11211")
parser.add_option_group(group)

group = OptionGroup(parser, "Output options")
group.add_option('-m', '--memcached', dest='memcached_ouput', action='store_true', default=False, help="Declare to enable uploading information to memcached.")
group.add_option('-o', '--outputfile', dest='out_file', action='store', default=None, help="Change where to ouput the data. Default: None")
parser.add_option_group(group)

options, args = parser.parse_args()


#############
## Classes ##
#############


###############
## Functions ##
###############

def xml_aggregate(xml_root, data):
    root = data.getroot()
    sub_element = copy.deepcopy(root[0])
    xml_root.append(sub_element)

def xml_file_list(ugrconfig="/etc/ugr/conf.d/endpoints.conf", directory=None, dfprefix=None, islocal=True):
    endpoints = {}
    file_list = []

    # Read the configuration file and extract the plugin URL and it's options.
    # The temp_vals dict will be merged later using the endpoint name as key
    # as a nested dict, so we can have each enpoints options together.
    with open(ugrconfig, "r") as _file:
        for line in _file:
            line = line.strip()
            if not line.startswith("#"):

                if "glb.locplugin[]" in line:
                    key, val = line.split(" ")[2::2]
                    endpoints.setdefault(key, {}).update({'url':val.strip()})
                    endpoints[key]['url'] = val.strip()

                elif "locplugin" in line:
                    x, val = line.partition(":")[::2]
                    key, option = x.split(".", 2)[1:]
                    endpoints.setdefault(key, {}).update({option:val.strip()})

                else: pass
                    #print( "I don't know what to do with %s", line)

    for endpoint, options in endpoints.items():
        if islocal:
            file_list.append(directory + "/" + endpoint + ".xml")
        else:
            mountpoint = options['xlatepfx'].split(" ")[0]
            file_list.append(dfprefix + mountpoint + "/admin/" + endpoint + '.xml')

    return file_list


def upload_to_memcached (x, memcached_ip='127.0.0.1', memcached_port='11211'):
    import memcache
    memcached_srv = memcached_ip + ':' + memcached_port
    mc = memcache.Client([memcached_srv])
    mc.set('Ugrstoragestats', x)

def memcached_builder(data, namespaces=None):
    xml_root = data.getroot()
    storagestats = ''
    endpoint = xml_root.find('sr:StorageUsageRecord/sr:RecordIdentity', namespaces=NSMAP).get(SR+'recordId')
    quota = xml_root.findtext('sr:StorageUsageRecord/sr:ResourceCapacityAllocated', namespaces=NSMAP)
    free = (int(xml_root.findtext('sr:StorageUsageRecord/sr:ResourceCapacityAllocated', namespaces=NSMAP))
            - int(xml_root.findtext('sr:StorageUsageRecord/sr:ResourceCapacityUsed', namespaces=NSMAP)))

    storagestats += '%%'.join([endpoint, quota, str(free)]) + '&&'
    return storagestats

###############
## Main Code ##
###############
# Temporary vars for testing purposes
#out_file = '/home/fernando/lab/dynafed-storage-scripts/star-agg/samples/' + 'dynafed.xml'
#xml_files = glob.glob(directory +"/*.xml")
#options.hostname = 'https://atlas-fed-fe1.triumf.ca/myfed'

# Define extensions that files containing storage stats will use.
extensions = ['.xml, .json']

# Aggregate the information fomr the files specified in the textfile provided
# by -f or --files.
if options.islocal and options.files:
    #print("Using list from text file")
    xml_files = []
    try:
        with open(options.files, 'r') as f:
            for line in f:
                line = line.strip()
                xml_files.append(line)
    except IOError:
        print("\nCould not read file:", options.files, '\n')
        sys.exit(1)

# Use all .xml files in the given directory provided by -d or --dir.
elif options.islocal and not options.ugrconfig:
    #print("Globbing from directory")
    xml_files = glob.glob(options.directory +"/*.xml")

# Extract the name from the endpoints.conf file. This works for URL remote location
# or to search for the files in the given directory.
else:
    #print("Using ugrconfig")
    xml_files = xml_file_list(ugrconfig=options.ugrconfig,
                              directory=options.directory,
                              dfprefix=options.dfprefix,
                              islocal=options.islocal
                             )

#print (xml_files)
storagestats = ''


# Define custon "sr:" namespace
SR_namespace = "http://eu-emi.eu/namespaces/2011/02/storagerecord"
SR = "{%s}" % SR_namespace
NSMAP = {"sr": SR_namespace}

# Create root element
xml_root = etree.Element(SR+"StorageUsageRecords", nsmap=NSMAP)
#print (xml_files)

# Parse all .xml files and aggregate them into the root element
for xml_file in xml_files:
    #print (xml_file)
    if options.islocal:
        data = etree.parse(xml_file)

    else:
        #print (options.hostname + xml_file)
        response = requests.get(options.hostname + xml_file,
                                stream=True,
                                verify=options.sslverify,
                                cert=(options.cert, options.key)
                               )
        #print(response.request.body)
        #print(response.request.headers)
        data = etree.parse(response.raw)

    storagestats += memcached_builder(data, NSMAP)
    xml_aggregate(xml_root, data)

if options.memcached_ouput:
    # Populate memcached
    upload_to_memcached(storagestats, options.memhost, options.memport)

if options.out_file:
    # If option is not specified, stdout is used. Anything else, it is assumed
    # to be a filename.
    xml_tree = etree.ElementTree(xml_root)
    if options.out_file == "stdout":
        print(etree.tostring(xml_tree, pretty_print=True))
    elif options.out_file:
        xml_tree.write(options.out_file, encoding="UTF-8", xml_declaration=True, method="xml")
