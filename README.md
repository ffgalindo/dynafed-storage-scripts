# Contents of project:

### staragg.py

Script intended to be run in an Dynafed host periodically to
aggregate storage accounting information provided by storage endpoints.
The aggregate will be output as StAR .xml file, .json file(not yet implemented),
and uploaded to the host's memcache instance.

#### stars3.py

Script that obtains bucket storage status information from a Ceph
RadosGW S3 gateway. To work, the gateway needs to have the admin API enabled
and user with bucket read "caps".
